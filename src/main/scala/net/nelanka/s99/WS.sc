import java.util.concurrent.ConcurrentHashMap

trait StringDeserializer[T] {
  def deserialize(s: String): T
}

trait StringSerializable[T] {
  def serialize: String
}

class CacheProxy[K <: StringSerializable[K], V <: StringSerializable[V]]
(cache: ConcurrentHashMap[String, String])(implicit d: StringDeserializer[V]) {
  def put(key: K, value: V) = cache.put(key.serialize, value.serialize)
  def get(key: K): V = d.deserialize(cache.get(key.serialize))
}

case class Name(firstName: String, lastName: String) extends StringSerializable[Name] {
  override def serialize: String = s"$firstName#$lastName"
}

case class ContactInfo(phoneNumber: String, email: String) extends StringSerializable[ContactInfo] {
  override def serialize: String = s"$phoneNumber#$email"
}

implicit object NameDeserializer extends StringDeserializer[Name] {
  val delimiter = '#'
  override def deserialize(s: String): Name = {
    val Array(firstName, lastName) = s.split(delimiter)
    Name(firstName, lastName)
  }
}

implicit object ContactInfoDeserializer extends StringDeserializer[ContactInfo] {
  val delimiter = '#'
  override def deserialize(s: String): ContactInfo = {
    val Array(phoneNumber, email) = s.split(delimiter)
    ContactInfo(phoneNumber, email)
  }
}

val cache = new ConcurrentHashMap[String, String]()
val proxy = new CacheProxy[Name, ContactInfo](cache)

proxy.put(Name("Nelanka", "Perera"), ContactInfo("917-653-1400", "nelanka@gmail.com"))
proxy.get(Name("Nelanka", "Perera"))
