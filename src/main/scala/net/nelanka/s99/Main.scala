package net.nelanka.s99

object Main extends App {

  import java.util.concurrent.ConcurrentHashMap

  trait StringDeserializer[T] {
    def deserialize(s: String): T
  }

  trait StringSerializable[T] {
    def serialize: String
  }

  class CacheProxy[K <: StringSerializable[K], V <: StringSerializable[V]]
  (cache: ConcurrentHashMap[String, String])(implicit d: StringDeserializer[V]) {
    def put(key: K, value: V) = cache.put(key.serialize, value.serialize)

    def get(key: K): Option[V] = Option(cache.get(key.serialize)).map(d.deserialize)
  }

  object Names {
    val delimiter = '#'

    case class Name(firstName: String, lastName: String) extends StringSerializable[Name] {
      override def serialize: String = s"$firstName$delimiter$lastName"
    }

    implicit object NameDeserializer extends StringDeserializer[Name] {
      override def deserialize(s: String): Name = {
        val Array(firstName, lastName) = s.split(delimiter)
        Name(firstName, lastName)
      }
    }

  }

  object ContactInfos {
    val delimiter = '#'

    case class ContactInfo(phoneNumber: String, email: String) extends StringSerializable[ContactInfo] {
      override def serialize: String = s"$phoneNumber$delimiter$email"
    }

    implicit object ContactInfoDeserializer extends StringDeserializer[ContactInfo] {
      override def deserialize(s: String): ContactInfo = {
        val Array(phoneNumber, email) = s.split(delimiter)
        ContactInfo(phoneNumber, email)
      }
    }
  }

  import Names._
  import ContactInfos._

  val cache = new ConcurrentHashMap[String, String]()
  val proxy = new CacheProxy[Name, ContactInfo](cache)

  proxy.put(Name("Nelanka", "Perera"), ContactInfo("917-653-1400", "nelanka@gmail.com"))
  println(proxy.get(Name("Nelanka", "Perera")))
  proxy.put(Name("Nelanka", "Perera"), ContactInfo("917-653-1400", "nelanka@hotmail.com"))
  println(proxy.get(Name("Nelanka", "Perera")))
  println(proxy.get(Name("Rachel", "Perera")))

  val Array(first, second, _*) = "hello#world#too".split('#')

  println(s"first: $first")
  println(s"second: $second")

  val a :: b :: c = "hello#world#too#foo#bar".split('#').toList
  println(s"a: $a")
  println(s"b: $b")
  println(s"c: $c")

  val List(aa, bb, cc, _*) = "hello#world#too#foo#bar".split('#').toList
  println(s"aa: $aa")
  println(s"bb: $bb")
  println(s"cc: $cc")
}
