//val Array(first, second) =
  "hello#world#too".split('#')


//def f[T: Acceptable](t: T) = t

//That weird declaration of T is called a context bound in Scala lingo. It's new to Scala 2.8, precisely to make this stuff easier. What it actually does is automatically define an implicit parameter -- and, for that very reason, it cannot be combined with other implicit parameters, though you can always use the full syntax to get the same result. Here's the full syntax:

//def f[T](t: T)(implicit ev: Acceptable[T]) = t
